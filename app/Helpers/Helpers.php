<?php

if (! function_exists('stEcho')) {
    /**
     * @comment reusable helper to print in test case to show results
     * @param $l
     * @return false|int
     */
    function stEcho($l)
    {
        return fwrite(STDERR, print_r($l . PHP_EOL, TRUE));
    }
}
