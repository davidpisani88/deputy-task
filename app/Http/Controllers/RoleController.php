<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Roles;

class RoleController extends Controller
{
    /**
     * @return Roles[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getRoles(){
        $roles = Roles::all();
        return $roles;
    }

}
