<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Roles;

class UserController extends Controller
{

    /**
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getUsers(){
        $users = User::all();
        return $users;
    }

    /**
     * @param $id
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getSubOrdinates($id)
    {
        $roles = Roles::subordinates($id);
        $users = User::whereIn('role', collect($roles)->pluck('id'))->get();
        return $users;
    }
}
