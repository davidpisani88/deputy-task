<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Roles extends Model
{
    use HasFactory;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentRoles()
    {
        return $this->belongsTo(Roles::class, 'parent', 'id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childRoles()
    {
        return $this->hasMany(Roles::class, 'parent', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class, 'role', 'id');
    }

    /**
     * @comment I chose to use raw sql statement as eloquent wasn't allowing
     * for recursive querying without the use of several foreach loops after
     * getting the result set. This was my cleanest solution to output as desired.
     *
     * The variable @pid relates to the parent role id
     *
     * @param $id
     * @return mixed
     */
    public static function subordinates($id)
    {
        $recursiveQuery = "select  id, name, parent 
                           from    (select * from roles order by parent, id) roles_sorted,
                            (select @pid := '$id') initialisation
                            where   find_in_set(parent, @pid)
                            and     length(@pid := concat(@pid, ',', id))";
        $result =  DB::select($recursiveQuery);
        return $result;
    }



}
