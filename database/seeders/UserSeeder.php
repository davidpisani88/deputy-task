<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
          [
              array(
                  'name' => 'Adam Admin',
                  'role' => 1
              ),
              array(
                  'name' => 'Mary Manager',
                  'role' => 2
              ),
              array(
                  'name' => 'Sam Supervisor',
                  'role' => 3
              ),
              array(
                  'name' => 'Emily Employee',
                  'role' => 4
              ),
              array(
                  'name' => 'Steve Trainer',
                  'role' => 5
              )
          ]
        );
    }
}
