<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
           [
               array(
                   'name' => 'System Administrator',
                   'parent' => 0
               ),
               array(
                   'name' => 'Location Manager',
                   'parent' => 1
               ),
               array(
                   'name' => 'Supervisor',
                   'parent' => 2
               ),
               array(
                   'name' => 'Employee',
                   'parent' => 3
               ),
               array(
                   'name' => 'Trainer',
                   'parent' => 3
               )
           ]
        );
    }
}
