<?php

namespace Tests\Unit;

use Tests\TestCase;

class GetSubordinatesTest extends TestCase
{
    public function test_getSubOrdinates(){
        stEcho('Testing getSubOrdinates(3)');
        $response = $this->get('/getSubOrdinates/3');
        stEcho($response->content());
        $response->assertStatus(200);

        stEcho('Testing getSubOrdinates(1)');
        $response = $this->get('/getSubOrdinates/1');
        stEcho($response->content());
        $response->assertStatus(200);
    }
}
