## Deputy coding challenge

Welcome to my challenge from Deputy

## Install and Setup

To install and run please follow the commands below:
```
$ git clone https://davidpisani88@bitbucket.org/davidpisani88/deputy-task.git
$ cd deputy-task
$ composer install
$ cp .env.example .env
```

After that we need to create MySQL schema `deputy_task` and update the `.env` with your environments database credentials

``CREATE SCHEMA `deputy_task` ;``

```
$ php artisan key:generate
$ php artisan config:cache
$ php artisan migrate
$ php artisan migrate --seed
$ php artisan serve
```

Now your environment is setup you should be able to go to your browser and go to `http://localhost:8000/`. Here you will be presented with the welcome page.

## Test suite

To run tests go to your command line, make sure your are in the root directory and run  `./vendor/bin/phpunit`

## Endpoints available

```
http://localhost:8000/getUsers
http://localhost:8000/getRoles
http://localhost:8000/getSubOrdinates/{user_id}
http://localhost:8000/getSubOrdinates/1
```
